//
//  HalifaxRequest.swift
//
//  Copyright (c) 2019 Tak Tsubota All rights reserved.
//

import Foundation

// handles Halifax requests, manages url session tasks (single session for now)
class HalifaxRequest {
    
    private let halifaxClient = AAVHalifaxClient()
    private var sessionTask: URLSessionTask?
    
    public func getBranches(completion: @escaping (_ branches: Array<Branch>?, _ error: Error?) -> Void)  {
        // only allow single task at a time
        sessionTask?.cancel()
        
        sessionTask = halifaxClient.getBranches { [weak self] data, response, error in
            defer {
                self?.sessionTask = nil
            }
            if let error = error {
                self?.handleRequestError(error, message: nil, completion: completion)
            }
            else if let statusCode = response?.statusCode {
                if statusCode == 200 {
                    if let data = data {
                        HalifaxJSONDecoder.branchesFromJsonData(data, completion: completion)
                    }
                }
                else {
                    self?.handleRequestError(nil, message: nil, completion: completion)
                }
            }
            else {
                self?.handleRequestError(nil, message: nil, completion: completion)
            }
        }
        sessionTask?.resume()
    }
    
    private func handleRequestError(_ error: Error?, message: String?, completion: @escaping (_ branches: Array<Branch>?, _ error: Error?) -> Void) {
        completion(nil, error)
    }
}

