//
//  HalifaxJSONDecoder.swift
//
//  Copyright (c) 2019 Tak Tsubota All rights reserved.
//

import Foundation

fileprivate struct MetaPayloadData: Decodable {
    let totalResults: Int
    enum CodingKeys: String, CodingKey {
        case totalResults = "TotalResults"
    }
}

fileprivate struct BrandPayloadData: Decodable {
    let brand: Array<Brand>
    enum CodingKeys: String, CodingKey {
        case brand = "Brand"
    }
    
}

fileprivate struct BranchesPayloadData: Decodable {
    let meta: MetaPayloadData
    let data: Array<BrandPayloadData>
}

struct HalifaxJSONDecoder {
    
    static func branchesFromJsonData(_ jsonData: Data, completion: @escaping (_ branches: Array<Branch>?, _ error: Error?) -> Void) {
        do {
            let decoder = JSONDecoder()
            let branchesData = try decoder.decode(BranchesPayloadData.self, from: jsonData)
            
            // only supports single brand branches
            if branchesData.data.count > 0 && branchesData.data[0].brand.count > 0 {
                completion(branchesData.data[0].brand[0].branches, nil)
            }
            else {
                completion(nil, nil)
            }
        }
        catch {
            completion(nil, error)
        }
    }
}



