//
//  ContactInfo.swift
//
//  Copyright (c) 2019 Tak Tsubota All rights reserved.
//

import Foundation

struct ContactInfo: Decodable {
    let contactType: String
    let contactContent: String
    
    enum CodingKeys: String, CodingKey {
        case contactType = "ContactType"
        case contactContent = "ContactContent"
    }
}

