//
//  PostalAddress.swift
//
//  Copyright (c) 2019 Tak Tsubota All rights reserved.
//

import Foundation

struct PostalAddress: Decodable {
    let addressLine: Array<String>
    let townName: String?
    let countrySubDivision: Array<String>?
    let country: String
    let postCode: String
    enum CodingKeys: String, CodingKey {
        case addressLine = "AddressLine"
        case townName = "TownName"
        case countrySubDivision = "CountrySubDivision"
        case country = "Country"
        case postCode = "PostCode"
    }
}

extension PostalAddress {
    var addressMultiLineDisplayString: String {
        var address = ""
        for addressLine in addressLine {
            if address.count > 0 {
                address.append("\n")
            }
            address.append(addressLine.capitalized)
        }
        
        if address.count > 0 {
            address.append("\n")
        }
        if let townName = self.townName {
            address.append("\(townName.capitalized) \(postCode)")
        }
        else {
            address.append(postCode)
        }
        
        return address
    }
}


