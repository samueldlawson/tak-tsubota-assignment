//
//  Brand.swift
//
//  Copyright (c) 2019 Tak Tsubota All rights reserved.
//

import Foundation

struct Brand: Decodable {
    let brandName: String
    let branches: Array<Branch>
    enum CodingKeys: String, CodingKey {
        case brandName = "BrandName"
        case branches = "Branch"
    }
}

