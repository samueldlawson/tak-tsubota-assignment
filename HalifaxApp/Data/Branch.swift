//
//  Branch.swift
//
//  Copyright (c) 2019 Tak Tsubota All rights reserved.
//

import Foundation

struct Branch: Decodable {
    let name: String
    let contactInfo: Array<ContactInfo>
    let postalAddress: PostalAddress
    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case contactInfo = "ContactInfo"
        case postalAddress = "PostalAddress"
    }
}

extension Branch {
    var nameDisplayString: String {
        return name.capitalized
    }
}

struct Branches {
    
    private var branches: Array<Branch> = []
    private var filteredBranches: Array<Branch> = []
    private var filterString: String = ""
    
    func numberOfBranches(filtered: Bool) -> Int {
        return filtered ? filteredBranches.count : branches.count
    }
    
    func branchAt(index: Int, filtered: Bool) -> Branch {
        return filtered ? filteredBranches[index] : branches[index]
    }
    
    mutating func setBranches(_ branches: Array<Branch>) {
        self.branches = branches.sorted(by: { $0.name < $1.name })
        filterBranches(filterString)
    }
    
    mutating func filterBranches(_ filterString: String) {
        self.filterString = filterString
        filteredBranches = branches.filter({( branch : Branch) -> Bool in
            if let townName = branch.postalAddress.townName {
                return townName.lowercased().hasPrefix(filterString.lowercased())
            }
            else {
                return false
            }
        })
    }
    
}


