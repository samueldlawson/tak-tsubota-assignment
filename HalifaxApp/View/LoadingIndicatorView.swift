//
//  LoadingIndicatorView.swift
//
//  Copyright (c) 2019 Tak Tsubota All rights reserved.
//

import UIKit

class LoadingIndicatorView: UIView {
    
    private(set) var spinnerView: UIActivityIndicatorView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .clear
        self.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        // -- spinner
        spinnerView = UIActivityIndicatorView(style: .gray)
        spinnerView.backgroundColor = .clear
        self.addSubview(spinnerView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        spinnerView.frame.origin.x = (self.bounds.size.width - spinnerView.frame.size.width) / 2
        spinnerView.frame.origin.y = 40
    }
    
}

