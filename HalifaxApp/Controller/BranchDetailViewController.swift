//
//  BranchDetailViewController.swift
//
//  Copyright (c) 2019 Tak Tsubota All rights reserved.
//

import UIKit

class BranchDetailViewController: UITableViewController {
    
    var branch: Branch?
    
    fileprivate enum Form {
        enum Section {
            static let Address = 0
            static let Contact = 1
            static let count = 2
        }
        enum AddressRow {
            static let Address = 0
            static let count = 1
        }
        enum ContactRow {
            static let Contact = 0
            static let count = 0 // dynamic
        }
    }
    
    fileprivate static let addressCellIdentifier = "AddressCell"
    fileprivate static let contactCellIdentifier = "ContactCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if let branch = branch {
            self.title = branch.nameDisplayString
        }
        
        self.tableView.allowsSelection = false
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableView.automaticDimension
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Form.Section.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows = 0
        
        if section == Form.Section.Address {
            numberOfRows = Form.AddressRow.count
        }
        else if section == Form.Section.Contact {
            numberOfRows = Form.ContactRow.count
            if let branch = self.branch {
                numberOfRows += branch.contactInfo.count
            }
        }
        
        return numberOfRows;
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var title: String? = nil
        if section == Form.Section.Address {
            title = NSLocalizedString("Address", comment:"")
        }
        else if section == Form.Section.Contact {
            title = NSLocalizedString("Contact Info", comment:"")
        }
        return title
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        
        if indexPath.section == Form.Section.Address {
            cell = self.tableView(tableView, addressCellForRowAt: indexPath)
        }
        else if indexPath.section == Form.Section.Contact {
            cell = self.tableView(tableView, contactCellForRowAt: indexPath)
        }
        
        guard cell != nil else {
            fatalError("Unexpected cell type at \(indexPath.section), \(indexPath.row)")
        }
        
        return cell!
    }
    
    fileprivate func tableView(_ tableView: UITableView, addressCellForRowAt indexPath: IndexPath) -> UITableViewCell? {
        let cell = tableView.dequeueReusableCell(withIdentifier: BranchDetailViewController.contactCellIdentifier) ?? UITableViewCell(style: .subtitle, reuseIdentifier: BranchDetailViewController.contactCellIdentifier)
        
        cell.textLabel?.textAlignment = .left
        cell.textLabel?.text = nil
        cell.textLabel?.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title3)
        cell.detailTextLabel?.textAlignment = .left
        cell.detailTextLabel?.text = nil
        cell.detailTextLabel?.numberOfLines = 0
        cell.detailTextLabel?.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.callout)
        cell.detailTextLabel?.textColor = .gray
        
        cell.accessoryType = .none
        
        if let branch = self.branch {
            cell.textLabel?.text = branch.nameDisplayString
            cell.detailTextLabel?.text = branch.postalAddress.addressMultiLineDisplayString
        }
        
        return cell
    }
    
    fileprivate func tableView(_ tableView: UITableView, contactCellForRowAt indexPath: IndexPath) -> UITableViewCell? {
        let cell = tableView.dequeueReusableCell(withIdentifier: BranchDetailViewController.addressCellIdentifier) ?? UITableViewCell(style: .value1, reuseIdentifier: BranchDetailViewController.addressCellIdentifier)
        
        cell.textLabel?.textAlignment = .left
        cell.textLabel?.text = nil
        cell.detailTextLabel?.text = nil
        cell.accessoryType = .none
        
        if let branch = self.branch {
            if indexPath.row < branch.contactInfo.count {
                cell.textLabel?.text = branch.contactInfo[indexPath.row].contactType
                cell.detailTextLabel?.text = branch.contactInfo[indexPath.row].contactContent
            }
        }
        
        return cell
    }
    
}
