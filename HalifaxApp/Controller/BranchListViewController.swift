//
//  BranchListViewController.swift
//
//  Copyright (c) 2019 Tak Tsubota All rights reserved.
//

import UIKit


class BranchListViewController : UITableViewController {
    
    fileprivate let halifaxRequest = HalifaxRequest()
    fileprivate var branches = Branches()
    
    fileprivate var loadingIndicatorView: LoadingIndicatorView? = nil
    fileprivate var refreshIndicatorControl: UIRefreshControl? = nil
    fileprivate let searchController = UISearchController(searchResultsController: nil)
    fileprivate static let mainCellIdentifier = "MainCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Halifax"
        
        // -- tableview
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        // -- search bar
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = NSLocalizedString("Search branches by city", comment: "")
        self.navigationItem.searchController = searchController
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.definesPresentationContext = true
        self.extendedLayoutIncludesOpaqueBars = true
        
        // -- pull to refresh
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshAction(_:)), for: .valueChanged)
        self.refreshIndicatorControl = refreshControl
        
        showLoadingIndicator()
        loadBranchesData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let detailViewController = BranchDetailViewController(style: .grouped)
        detailViewController.branch = branches.branchAt(index: indexPath.row, filtered: isFiltering())
        navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    // MARK: UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return branches.numberOfBranches(filtered: isFiltering())
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BranchListViewController.mainCellIdentifier) ?? UITableViewCell(style: .subtitle, reuseIdentifier: BranchListViewController.mainCellIdentifier)
        
        let branch = branches.branchAt(index: indexPath.row, filtered: isFiltering())
        
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = branch.nameDisplayString
        cell.textLabel?.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title3)
        
        cell.detailTextLabel?.numberOfLines = 0
        cell.detailTextLabel?.text = branch.postalAddress.addressMultiLineDisplayString
        cell.detailTextLabel?.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.callout)
        cell.detailTextLabel?.textColor = .gray
        
        return cell
    }
    
    // MARK: targetAction
    
    @objc func refreshAction(_ sender: UIRefreshControl) {
        loadBranchesData()
    }
    
    // MARK: -
    
    fileprivate func isFiltering() -> Bool {
        let searchBarIsEmpty = searchController.searchBar.text?.isEmpty ?? true
        return searchController.isActive && !searchBarIsEmpty
    }
    
    fileprivate func loadBranchesData() {
        halifaxRequest.getBranches { [weak self] branches, error in
            if let branches = branches {
                self?.branches.setBranches(branches)
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                    self?.branchesDataDidFinishLoading()
                }
            }
            else {
                DispatchQueue.main.async {
                    self?.branchesDataDidFinishLoadingWithError(error)
                }
            }
        }
    }
    
    fileprivate func presentDataLoadError(_ error: Error?) {
        print("error \(String(describing: error))")
        let alertController = UIAlertController(title: NSLocalizedString("Unable to load data", comment:""), message: error?.localizedDescription, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil)
        alertController.addAction(alertAction)
        self.navigationController?.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func branchesDataDidFinishLoadingWithError(_ error: Error?) {
        self.hideLoadingIndicator()
        self.presentDataLoadError(error)
    }
    
    fileprivate func branchesDataDidFinishLoading() {
        self.hideLoadingIndicator()
    }
    
    fileprivate func showLoadingIndicator() {
        if self.loadingIndicatorView == nil {
            let indicatorView = LoadingIndicatorView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
            self.view.addSubview(indicatorView)
            indicatorView.spinnerView.startAnimating()
            self.loadingIndicatorView = indicatorView
        }
    }
    
    fileprivate func hideLoadingIndicator() {
        if let indicatorView = self.loadingIndicatorView {
            indicatorView.spinnerView.stopAnimating()
            indicatorView.removeFromSuperview()
            self.loadingIndicatorView = nil
            self.refreshControl = self.refreshIndicatorControl
        }
        self.refreshControl?.endRefreshing()
    }
    
}

extension BranchListViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        branches.filterBranches(searchController.searchBar.text!)
        tableView.reloadData()
    }
    
}
