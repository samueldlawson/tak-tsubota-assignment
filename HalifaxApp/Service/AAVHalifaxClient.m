//
//  AAVHalifaxClient.m
//
//  Copyright (c) 2019 Tak Tsubota. All rights reserved.
//

#import "AAVHalifaxClient.h"

NSString * const AAVHalifaxAPIBaseURLString = @"https://api.halifax.co.uk/open-banking/v2.2/";

@interface AAVHalifaxClient ()
@property (nonatomic, strong, readwrite) NSURLSession *urlSession;
@end

@implementation AAVHalifaxClient

- (instancetype)init
{
    self = [super init];
    if (self != nil) {
        _urlSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    }
    return self;
}

- (NSURLSessionTask *)getBranchesWithCompletion:(void (^ __nullable)(NSData * _Nullable branchesData, NSHTTPURLResponse * _Nullable response, NSError * _Nullable error))completion
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", AAVHalifaxAPIBaseURLString, @"branches"]];
    NSURLSessionDataTask *dataTask = [self.urlSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (completion) {
                                                            completion(data, (NSHTTPURLResponse *)response, error);
                                                        }
                                                    }];
    return dataTask;
}

@end
