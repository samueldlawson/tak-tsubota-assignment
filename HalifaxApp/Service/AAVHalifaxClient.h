//
//  AAVHalifaxClient.h
//
//  Copyright (c) 2019 Tak Tsubota. All rights reserved.
//

#import <Foundation/Foundation.h>


NS_ASSUME_NONNULL_BEGIN
@interface AAVHalifaxClient : NSObject

- (NSURLSessionTask *)getBranchesWithCompletion:(void (^ __nullable)(NSData * _Nullable branchesData,
                                                                     NSHTTPURLResponse * _Nullable response,
                                                                     NSError * _Nullable error))completion
NS_SWIFT_NAME(getBranches(_:));

@end
NS_ASSUME_NONNULL_END

