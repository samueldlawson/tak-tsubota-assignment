//
//  HalifaxJSONDecoderTests.swift
//
//  Copyright (c) 2019 Tak Tsubota All rights reserved.
//

import XCTest

class HalifaxJSONDecoderTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testJson() {
        let json = """
        {
            "meta": {
                "TotalResults":1
            },
            "data": [
                {
                    "Brand":[
                        {
                            "BrandName":"Halifax",
                            "Branch":[
                                {
                                    "Identification":"11863000",
                                    "SequenceNumber":"00",
                                    "Name":"TADCASTER",
                                    "ContactInfo":[
                                        {
                                            "ContactType":"Phone",
                                            "ContactContent":"+44-1937835809"
                                        },
                                        {
                                            "ContactType":"Fax",
                                            "ContactContent":"+44-1937835674"
                                         }
                                    ],
                                    "PostalAddress":{
                                        "AddressLine":[
                                            "HALIFAX BRANCH 24 BRIDGE STREET"
                                        ],
                                        "TownName":"TADCASTER",
                                        "CountrySubDivision":["NORTH YORKSHIRE"],
                                        "Country":"GB",
                                        "PostCode":"LS24 9AL"
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }
        """.data(using: .utf8)!
        
        HalifaxJSONDecoder.branchesFromJsonData(json, completion: { branches, error in
            if let error = error {
                XCTFail("json error: \(error)")
            }
            else if let branchesData = branches {
                XCTAssertEqual(branchesData.count, 1, "Number of branches is wrong")
                let branch = branchesData[0]
                XCTAssertEqual(branch.name, "TADCASTER", "Branch name is wrong")
                XCTAssertEqual(branch.contactInfo.count, 2, "Number of contact info is wrong")
                let contact0 = branch.contactInfo[0]
                XCTAssertEqual(contact0.contactType, "Phone", "Contact type is wrong")
                let contact1 = branch.contactInfo[1]
                XCTAssertEqual(contact1.contactType, "Fax", "Contact type is wrong")
                XCTAssertNotNil(branch.postalAddress.townName, "townName is nil")
            }
            else {
                XCTFail("json unknown error")
            }
        })
    }

    func testJsonEmptyTownName() {
        let json = """
        {
            "meta": {
                "TotalResults":1
            },
            "data": [
                {
                    "Brand":[
                        {
                            "BrandName":"Halifax",
                            "Branch":[
                                {
                                    "Identification":"11863000",
                                    "SequenceNumber":"00",
                                    "Name":"TADCASTER",
                                    "ContactInfo":[
                                        {
                                            "ContactType":"Phone",
                                            "ContactContent":"+44-1937835809"
                                        },
                                        {
                                            "ContactType":"Fax",
                                            "ContactContent":"+44-1937835674"
                                         }
                                    ],
                                    "PostalAddress":{
                                        "AddressLine":[
                                            "HALIFAX BRANCH 24 BRIDGE STREET"
                                        ],
                                        "CountrySubDivision":["NORTH YORKSHIRE"],
                                        "Country":"GB",
                                        "PostCode":"LS24 9AL"
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }
        """.data(using: .utf8)!
        
        HalifaxJSONDecoder.branchesFromJsonData(json, completion: { branches, error in
            if let error = error {
                XCTFail("json error: \(error)")
            }
            else if let branchesData = branches {
                XCTAssertEqual(branchesData.count, 1, "Number of branches is wrong")
                let branch = branchesData[0]
                XCTAssertNil(branch.postalAddress.townName, "townName is not nil")
            }
            else {
                XCTFail("json unknown error")
            }
        })
    }

}
