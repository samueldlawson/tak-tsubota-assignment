//
//  BranchJsonTests.swift
//
//  Copyright (c) 2019 Tak Tsubota All rights reserved.
//

import XCTest

class BranchJsonTests: XCTestCase {

    var branches: Branches? = nil
    
    override func setUp() {
        super.setUp()

        let path = Bundle(for: type(of: self)).path(forResource: "HalifaxBranch", ofType: "json")
        let jsonData = try? Data(contentsOf: URL(fileURLWithPath: path!), options: .alwaysMapped)
        
        HalifaxJSONDecoder.branchesFromJsonData(jsonData!, completion: { branches, error in
            if let branchesData = branches {
                var decodedBranches = Branches()
                decodedBranches.setBranches(branchesData)
                self.branches = decodedBranches
            }
        })
    }

    override func tearDown() {
        super.tearDown()
    }

    func testFilterBranches() {
        XCTAssertNotNil(self.branches, "branches failed to load")
        if var branches = self.branches {
            branches.filterBranches("Liv")
            XCTAssertEqual(branches.numberOfBranches(filtered: true), 7, "Filter wrong results")
        }
        
    }
}
