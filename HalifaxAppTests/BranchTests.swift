//
//  BranchTests.swift
//
//  Copyright (c) 2019 Tak Tsubota All rights reserved.
//

import XCTest

class BranchTests: XCTestCase {

    override func setUp() {

    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testBranches() {
        let contact = ContactInfo(contactType: "Phone", contactContent: "+44-1937835809")
        let address0 = PostalAddress(addressLine: [""], townName: "TADCASTER", countrySubDivision: nil, country: "GB", postCode: "LS24 9AL")
        let branch0 = Branch(name: "branch0", contactInfo: [contact], postalAddress: address0)
        let address1 = PostalAddress(addressLine: [""], townName: "PRESCOT", countrySubDivision: nil, country: "GB", postCode: "LS24 9AL")
        let branch1 = Branch(name: "branch1", contactInfo: [contact], postalAddress: address1)

        var branches = Branches()
        branches.setBranches([branch0, branch1])
        XCTAssertEqual(branches.numberOfBranches(filtered: false), 2, "Incorrect number of branches")

    }

}
