#Aavri - iOS Developer Assignment

Tak Tsubota - tak.tsubota@gmail.com

## Dev Environment

* Xcode 10.2.1

* macOS 10.14.5

* iOS 12.2 (iPhone 6s)



## Build

Open `HalifaxApp.xcodeproj` in Xcode and build `HalifaxApp` scheme with appropriate device selected.


## Run

Select `Run` from the `Product` menu. To run the tests, select `Test` from the `Product` menu.

## App

This mobile application allows users to view and locate bank branches. The app allows users to filter the list of branches by city name.

## Architecture

This mobile application is designed with a layered architecture with each layer having specific responsibilities. The layers consist of service, data, and UI. The service layer is written in Objective-C. The data and the UI layers are written in Swift.

* Service Layer

	* The service layer handles the network communication with the Halifax API.

* Data Layer

	* The data layer consists of data models as well as a request class to manage network requests. The request class also transforms the json payload to the appropriate data models.
	
* UI Layer

	* The UI layer handles all the user interface elements for the application.
	
